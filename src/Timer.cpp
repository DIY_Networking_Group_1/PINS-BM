#include "Timer.h"

Timer::Timer(unsigned int timeout) {
    this->targetTimeout = timeout;
    this->lastTime = std::chrono::high_resolution_clock::now();
}

Timer::~Timer() {
}

void Timer::setTimer() {
    std::chrono::time_point<std::chrono::system_clock> tmp = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> diff = (tmp - this->lastTime);
    usleep((diff.count() * 1000000) < this->targetTimeout ? (this->targetTimeout - (diff.count() * 1000000)) : 0);
    this->lastTime = std::chrono::high_resolution_clock::now();
}
