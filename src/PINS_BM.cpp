#include "PINS_BM.h"


struct Args* parseParameter(int argc, char const *argv[]) {
    struct Args* temp = new Args();
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-cf") == 0) {
            if (argc > i + 1) {
                temp->cache_folder = std::string(argv[++i]);
            }
        } else if (strcmp(argv[i], "-p") == 0) {
            if (argc > i + 1) {
                temp->port = atoi(argv[++i]);
            }
        } else if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            std::cout << HELPTEXT << std::endl;
            exit(0);
        } else if (strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--Monitor-mode") == 0) {
            temp->monitorMode = true;
        } else if (strcmp(argv[i], "-s") == 0) {
            if (argc > i +1) {
                temp->serialPort = (argv[++i][0] == '/') ? std::string(argv[i]) : ("/dev/" + std::string(argv[i]));
            }
        } else if ((strcmp(argv[i], "-a") == 0)) {
            if (argc > ++i) {
                if (temp->address == NULL) {
                    temp->address = new unsigned char[6];
                }
                for (int b = 0; b < 6; b++) {
                    temp->address[b] = argv[i][b];
                }
            }
        }
    }
    return temp;
}

void monitorMode(std::string port) {
    ESP32COM* e = port.compare("") ? new ESP32COM() : new ESP32COM(port);
    e->monitorMode();
    delete e;
}

void signalHandler(int s) {
    infoMSG("Got signal " + std::to_string(s));
    terminateProgram();
    exit(0);
}

void terminateProgram() {
    if (!isTerminating) {
        isTerminating = true;
        infoMSG("Terminanting progrmm");
        pc->stopLoop();
    }
}

int main(int argc, char const *argv[]) {
    signal(SIGINT, signalHandler);
    Args* a = parseParameter(argc, argv);
    if (!a->monitorMode) {
        pc = new PINS_Core(a->port);
        if (a->address == NULL) {
            warnMSG("No Esp32 spezified");
        } else {
            pc->setAddress(a->address);
        }
        // Start PinsCore:
        pc->startLoop();

        // Start the MQTT client:
        ADDRESS addr = std::make_shared<std::array<unsigned char, 6>>();

        for (int i = 0; i < 6; i++) {
            addr.get()->data()[i] = i;
        }

        MQTTHelper helper(pc, addr);
        helper.start();

        // Wait for user input:
        isTerminating = false;
        std::string input;
        std::cout << "Type stop to stop program" << std::endl;
        do {
            std::cin >> input;
        }while(!input.compare("stop\n"));
        terminateProgram();
    } else {
        monitorMode(a->serialPort);
    }
    return 0;
}
