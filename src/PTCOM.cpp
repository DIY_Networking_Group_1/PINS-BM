#include "PTCOM.h"


PTCON::PTCON(unsigned int port, ADDR addr) {
    this->port = port;
    this->intState = Disconnected;
    setAddr(addr);
    PtMessage t(pt_ack_last_message);
    this->ack_Message = t.genMsg();
    this->ack_Length = t.getMsgLength();
    t = PtMessage(pt_no_messages);
    this->no_More_Messages = t.genMsg();
    this->no_More_Length = t.getMsgLength();
}

PTCON::~PTCON() {
    if (this->intState == Connected) {
        stopLoop();
    }
    delete[] this->ack_Message;
    delete[] this->setAddr_Message;
    delete[] this->no_More_Messages;
}

void PTCON::handleClient(int fd, MessageQueue<std::pair<unsigned char*, unsigned int>>* out, LastSendMessages* ls, std::atomic<bool>* isFinished) {
    *isFinished = false;
    if (fd > 0) {
        debugMSG("New PT-Device connected");
        unsigned char* buffer = new unsigned char[DEFAULT_READ_BLOCK_SIZE];
        int len;
        bool intLoop = true;
        do {
            len = read(fd, buffer, DEFAULT_READ_BLOCK_SIZE);
            PtMessage msg(buffer, len);
            if (msg.getState() == i_success) {
                switch (msg.getType()) {
                    case pt_get_base_addr: {
                        write(fd, this->setAddr_Message, this->setAddr_Length);
                        break;
                    }
                    case pt_send_message: {
                        msg.freePayload();
                        out->push(std::make_pair(msg.getPayload(), msg.getPayloadLength()));
                        write(fd, this->ack_Message, this->ack_Length);
                        break;
                    }
                    case pt_no_messages: {
                        intLoop = false;
                        break;
                    }
                    default: {
                        debugMSG("PT-Device message doesn't seem to be useful");
                        break;
                    }
                }
            } else {
                debugMSG("Can't parse message from PT-Device");
            }
        }while (this->isRunning && intLoop && len > 0);
        if (len < 0) {
            close(fd);
            debugMSG("Lost connection to PT-Device");
            delete[] buffer;
            *isFinished = true;
            close(fd);
            return;
        }
        unsigned int i = 0;
        intLoop = true;
        do {
            LSTYPE tT = ls->getElement(i);
            if (!tT.fst) {
                if (tT.lst.first != NULL) {
                    PtMessage tM(pt_send_message, tT.snt, tT.lst.first, tT.lst.second);
                    unsigned char* c = tM.genMsg();
                    write(fd, c, tM.getMsgLength());
                    debugMSG("Sending Message to PT-Device");
                    delete[] c;
                    bool run = true;
                    while (run) {
                        int len = 0;
                        if ((len = read(fd, buffer, DEFAULT_READ_BLOCK_SIZE)) <= 0) {
                            intLoop = false;
                            break;
                        }
                        PtMessage t(buffer, len);
                        if (t.getState() == i_success) {
                            switch (t.getType()) {
                                case pt_ack_last_message: {
                                    run = false;
                                    break;
                                }
                                case pt_ptm_full: {
                                    run = false;
                                    intLoop = false;
                                    break;
                                } default: {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            ls->unlockElement(i);
        }while (this->isRunning && (++i <= std::numeric_limits<unsigned char>::max()) && intLoop);
        debugMSG("Finished communication with PTM");
        write(fd, this->no_More_Messages, this->no_More_Length);
        close(fd);
        delete[] buffer;
    }
    *isFinished = true;
    return;
}

void PTCON::intLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out, LastSendMessages* ls) {
    if (this->intState == Connected) {
        if (listen(this->sockfd, 3) < 0) {
            this->intState = Error;
            errorMSG("Can't listen to sock");
        }
        while (this->isRunning) {
            int fd = accept(this->sockfd, reinterpret_cast<struct sockaddr*>(&address), reinterpret_cast<socklen_t*>(&this->addrlen));
            std::atomic<bool>* a = new std::atomic<bool>;
            this->ptCHandler.push_back(std::make_pair(a, new std::thread(&PTCON::handleClient, this, fd, out, ls, a)));
            // closeOldCON();
            }
    }
}

void PTCON::closeOldCON() {
    for (unsigned long int i = 0; i < this->ptCHandler.size(); i++) {
        if (this->ptCHandler.at(i).first) {
            debugMSG("Removing element from ptCHandler");
            this->ptCHandler.erase(this->ptCHandler.begin());
            this->ptCHandler[i].second->join();
            delete this->ptCHandler[i].second;
            delete this->ptCHandler[i].first;
        }
    }
}

void PTCON::connect() {
    if (this->intState != Connected && this->intState != Error) {
        this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (this->sockfd == -1) {
            this->intState = Error;
            errorMSG("Can't open socket");
        }
        this->address.sin_family = AF_INET;
        this->address.sin_addr.s_addr = INADDR_ANY;
        this->address.sin_port = htons(this->port);
        if (bind(this->sockfd, (struct sockaddr*) &address, sizeof(address)) == -1) {
            this->intState = Error;
            errorMSG("Can't bind Port, does anything else listen on this port?");
        }
        this->intState = Connected;
    }
}

state PTCON::getState() {
    return this->intState;
}

void PTCON::startLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out, LastSendMessages* ls) {
    if (this->intState == Disconnected) {
        connect();
    }
    if (this->intState == Connected) {
        this->isRunning = true;
        debugMSG("Launching PTM Loop");
        this->t_loop = new std::thread(&PTCON::intLoop, this, out, ls);
        return;
    }
}

void PTCON::stopLoop() {
    infoMSG("Stopping PTCOM loop");
    this->isRunning = false;
    if (this->intState == Connected) {
        close(this->sockfd);
        this->intState = Disconnected;
    }
    if (this->t_loop != NULL) {
        this->t_loop->join();
        delete this->t_loop;
    }
    while (this->ptCHandler.size() > 0) {
        closeOldCON();
    }
    this->t_loop = NULL;
}

void PTCON::setAddr(ADDR addr) {
    this->hostAddress = addr;
    PtMessage t(pt_send_base_addr, addr);
    delete[] this->setAddr_Message;
    this->setAddr_Message = t.genMsg();
    this->setAddr_Length = t.getMsgLength();
}
