#pragma once

#include <experimental/filesystem>
#include <utility>
#include <vector>
#include <thread>
#include <string>
#include <iostream>

#include "Logger.h"
#include "Serial.h"
#include "Consts.h"
#include "messages/serial/SerialMessage.h"
#include "messages/MessageQueue.h"
#include "messages/MessageQueue.cpp"

class ESP32COM {
 private:
    bool isConnected = false;
    bool isRunning = false;
    MessageQueue<std::pair<unsigned char*, unsigned int>> sendQueue;
    std::thread* t_loop = NULL;
    Serial SerialCon = Serial("", DEFAULT_BAUDRATE);
    void sendByte(unsigned char* c, unsigned int len);
    bool isCorrectESP();
 public:
    ESP32COM();
    explicit ESP32COM(std::string serialPort);
    ~ESP32COM();
    int findESP();
    void updateScreen(unsigned char* c, unsigned int len);
    void updateScreen(std::vector<unsigned char>* v);
    void changeADDR(ADDRESS newADDR);
    void sendMsg(ADDRESS dst, unsigned char* c, unsigned int len);
    void sendMsg(ADDRESS dst, std::vector<unsigned char>* v);
    void Loop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out);
    void startLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out);
    void stopLoop();
    void monitorMode();
};
