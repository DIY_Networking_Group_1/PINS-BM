#include "Cache.h"



unsigned char LastSendMessages::getNextPos() {
    return (this->curPos == std::numeric_limits<unsigned char>::max()) ? 0 : this->curPos + 1;;
}

void LastSendMessages::ackMSG(unsigned char seqNr) {
    this->lockArr[seqNr].lock();
    this->LM[seqNr].fst = true;
    this->lockArr[seqNr].unlock();
}

void LastSendMessages::addMSG(ADDRESS addr, unsigned char* payload, unsigned int length) {
    this->curPos = getNextPos();
    this->lockArr[this->curPos].lock();
    if (this->LM[getNextPos()].fst == false) {
        infoMSG("Replacing unacknowledged MSG Number");
    }
    this->LM[this->curPos].fst = false;
    if (this->LM[this->curPos].lst.first != NULL) {
        delete[] this->LM[this->curPos].lst.first;
    }
    this->LM[this->curPos].lst.first = payload;
    this->LM[this->curPos].lst.second = length;
    this->LM[this->curPos].snt = addr;
    this->lockArr[this->curPos].unlock();
}

void LastSendMessages::addPing(ADDRESS addr) {
    this->addMSG(addr, NULL, 0);
}

unsigned char LastSendMessages::getPos() {
    return this->curPos;
}

LSTYPE LastSendMessages::getElement(unsigned char pos) {
    this->lockArr[pos].lock();
    return this->LM[pos];
}

void LastSendMessages::unlockElement(unsigned char pos) {
    this->lockArr[pos].unlock();
}


bool KnownHosts::exists(unsigned char* in) {
    for (unsigned int i = 0; i < this->kH.size(); i++) {
        if (compArr(kH[i].get()->data(), in, 6)) {
            return true;
        }
    }
    return false;
}

bool KnownHosts::exists(ADDRESS in) {
    return exists(in.get()->data());
}

void KnownHosts::add(ADDRESS in) {
    if (!exists(in)) {
        this->kH.push_back(in);
    }
}

unsigned int KnownHosts::size() {
    return this->kH.size();
}

bool LastRecievedMessages::add(unsigned char seqNr, ADDRESS SRCAddr, ADDRESS DSTAddr) {
    for (unsigned int i = 0; i < length; i++) {
        if (this->mArr[i].fst == seqNr) {
            if (compArr(this->mArr[i].snt.get()->data(), SRCAddr.get()->data(), 6)) {
                if (compArr(this->mArr[i].lst.get()->data(), DSTAddr.get()->data(), 6)) {
                    continue;
                }
            }
        }
        return false;
    }
    this->mArr[currPos] = Trible<unsigned char, ADDRESS, ADDRESS>(seqNr, SRCAddr, DSTAddr);
    this->length = length < DEFAULT_ARRAY_SIZE  ? length + 1 : length;
    this->currPos = currPos == length -1 ? 0 : currPos + 1;
    return true;
}


