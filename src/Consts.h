#pragma once

#include <string>
#include <memory>

// Main
// const std::string helpText = "PINS_BM -p <listening port> -c <cache size>\n\t-p changes the default port \n\t-c changes the chache size\n\t-h prints this message\n";
#define DEFAULT_PORT 1234
#define HELPTEXT "PINS_BM -p <listening port>\n\t-a <used address>\n\t-cf <folder where the cache is>\n\t-p changes the default port \n\t-c changes the chache size\n\t-m launches monitor mode\n\t-h prints this message\n"
#define DEFAULT_CACHE_FOLDER "/VAR/PIMS"

// PinsCore
#define DEFAULT_SLEEP 150000

// SERIAL
// should be greater than 100
#define DEFUALT_SERIAL_TIMEOUT 1500
#define DEFAULT_SERIAL_PAUSE 10000
#define SERIAL_READ_BLOCK 1024

// ESP32CON
#define DEFAULT_BAUDRATE B115200
#define MAXSEARCH 10
#define DEFAULTSERIALPREFIX "/dev/ttyUSB"
#define INTIAL_WRITE_FAIRNESS 1

// CACHE
#define DEFAULT_ARRAY_SIZE 100

// TYPES
typedef std::shared_ptr<std::array<unsigned char, 6>> ADDRESS;

// PTCOM
#define DEFAULT_READ_BLOCK_SIZE 1024
#define INITIAL_HANDLER_SIZE 32

// DEMOFILE
#define DEMO_FILE "/tmp/PINS_BM-out.txt"
