#pragma once

#include <vector>
#include <memory>
#include <utility>
#include <limits>
#include <mutex>

#include "Consts.h"
#include "Logger.h"
#include "messages/MessageTools.h"
#include "messages/MessageQueue.cpp"
#include "messages/MessageQueue.h"
#include "messages/toplevel/TopLevelMessage.h"

#define LSTYPE Trible<bool, ADDRESS, std::pair<unsigned char*, unsigned int>>

template <typename T1, typename T2, typename T3>

struct Trible {
    T1 fst;
    T2 snt;
    T3 lst;
    Trible<T1, T2, T3>(T1 fst, T2 snt, T3 lst):fst(fst), snt(snt), lst(lst) {}
    Trible<T1, T2, T3>() {
    }
};


struct KnownHosts {
    std::vector<ADDRESS> kH;
    bool exists(ADDRESS in);
    bool exists(unsigned char* in);
    void add(ADDRESS input);
    unsigned int size();
};

class LastSendMessages {
 private:
    std::array<std::mutex, std::numeric_limits<unsigned char>::max() + 1> lockArr{};
    std::array<LSTYPE, std::numeric_limits<unsigned char>::max() + 1> LM{};
    unsigned char curPos = 0;
 public:
    unsigned char getNextPos();
    void ackMSG(unsigned char seqNr);
    void addMSG(ADDRESS, unsigned char*, unsigned int);
    void addPing(ADDRESS addr);
    unsigned char getPos();
    LSTYPE getElement(unsigned char);
    void unlockElement(unsigned char);
};


class LastRecievedMessages {
 private:
    Trible<unsigned char, ADDRESS, ADDRESS> mArr[DEFAULT_ARRAY_SIZE];
    unsigned int currPos = 0;
    unsigned int length = 0;
 public:
    bool add(unsigned char seqNr, ADDRESS SRCAddr, ADDRESS DSTAddr);
};

