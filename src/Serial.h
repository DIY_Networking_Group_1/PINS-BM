#pragma once

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <cstring>
#include <vector>
#include <string>

#include "Enums.h"
#include "Timer.h"
#include "Logger.h"
#include "Consts.h"


class Serial {
 private:
    speed_t baudrate;
    unsigned int timeout = DEFUALT_SERIAL_TIMEOUT;
    std::string SerialPort;
    int fd = 0;
    Timer readTimer = Timer(DEFAULT_SERIAL_PAUSE);
    state curState;
    termios getTTY();
    void configTTY(termios* tty);

 public:
    Serial(const std::string, speed_t);
    Serial(const std::string, speed_t, unsigned int);
    ~Serial();
    int Open();
    void Close();
    int Read(unsigned char* c);
    const std::string Read();
    int Write(std::vector <unsigned char>* v);
    int Write(unsigned char* c, unsigned int length);
    state getState();
};
