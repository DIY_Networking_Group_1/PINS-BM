#include "MQTTHelper.h"

MQTTHelper::MQTTHelper(PINS_Core* pc, KiliStpAddr localAddr): localAddr(localAddr) {
    this->pc = pc;
}

MQTTHelper::~MQTTHelper() {
    stop();
}

void MQTTHelper::start() {
    if (!running) {
        infoMSG("Starting MQTT client...");
        running = startInternal();
    } else {
        warnMSG("Unable to start MQTT client. Client running!");
    }
}

void MQTTHelper::stop() {
    if (running) {
        infoMSG("Stopping MQTT client...");
        stopInternal();
        running = false;
    } else {
        warnMSG("Unable to stop MQTT client. Client not running!");
    }
}

bool MQTTHelper::isRunning() {
    return running;
}

bool MQTTHelper::startInternal() {
    MQTTClient_create(&client, MQTT_SERVER_ADDRESS, const_cast<const char*>(reinterpret_cast<char*>(localAddr.get()->data())), MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conOpt.keepAliveInterval = 20;
    conOpt.cleansession = 1;

    MQTTClient_setCallbacks(client, pc, onConnLost, onArrived, onDelivered);

    int result = MQTTClient_connect(client, &conOpt);
    if (result != MQTTCLIENT_SUCCESS) {
        debugMSG("MQTT unable to connect.");
        return false;
    }
    infoMSG("MQTT connected to server.");
    MQTTClient_subscribe(client, MQTT_TOPIC, MQTT_QOS);
    return true;
}

void MQTTHelper::stopInternal() {
    MQTTClient_unsubscribe(client, MQTT_TOPIC);
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
}

int MQTTHelper::onArrived(void* context, char* topicName, int topicLen, MQTTClient_message* message) {
    PINS_Core* pc = reinterpret_cast<PINS_Core*>(context);
    int realTopicLen = topicLen;
    if (topicLen <= 0) {  // https://github.com/eclipse/paho.mqtt.c/issues/440#issuecomment-379710094
        realTopicLen = strlen(topicName);
    }
    std::string json = formatToJson(topicName, realTopicLen, reinterpret_cast<char*>(message->payload), message->payloadlen);
    debugMSG("MQTT JSON msg: " + json);
    pc->sendBCMSG(reinterpret_cast<uchar*>(const_cast<char*>(json.c_str())), json.length());

    std::string s = std::string(reinterpret_cast<char*>(message->payload), message->payloadlen);
    debugMSG("MQTT message for topic " + std::string(topicName, realTopicLen) + " received: " + std::string(s));

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

std::string MQTTHelper::formatToJson(char* topic, int topicLength, char* data, int dataLength) {
    return "{ \"topic\": \"" + std::string(topic, topicLength) + "\", \"data\": \"" + std::string(data, dataLength) + "\"}";
}

void MQTTHelper::onConnLost(void* context, char* cause) {
    warnMSG("MQTT connection lost. Cause: " + std::string(cause));
}

void MQTTHelper::onDelivered(void* context, MQTTClient_deliveryToken dt) {
    debugMSG("MQTT message delivered.");
}
