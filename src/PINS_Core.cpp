#include "PINS_Core.h"


PINS_Core::PINS_Core(unsigned int port) {
    this->pt = new PTCON(port, this->addr);
}

PINS_Core::~PINS_Core() {
    if (isRunning)
        stopLoop();
    if (this->esp != NULL) {
        this->esp->stopLoop();
    }
    if (this->pt != NULL) {
        this->pt->stopLoop();
    }
    delete this->pt;
    delete this->esp;
}

void PINS_Core::processMsg(unsigned char* c, unsigned int i) {
    debugMSG("Recieved Message" + std::string(reinterpret_cast<char*>(c), i));
    TopMsg t(c, i);
    if (t.getState() == i_error) {
        return;
    } else {
        if (compArr(this->addr.get()->data(), t.tmpGetDest(), 6)) {
            switch (t.getType()) {
                case to_ping: {
                    TopMsg ret = TopMsg(to_ack, t.getSRC(), this->addr, t.getSeqNr());
                    this->kH.add(t.getSRC());
                    this->esp->sendMsg(t.getSRC(), ret.genMsg(), ret.getMsgLength());
                    break;
                }
                case to_ack:
                    this->lsM.ackMSG(t.getSeqNr());
                    break;
                 case to_sendbytes: {
                    TopMsg ret = TopMsg(to_ack, t.getSRC(), this->addr, t.getSeqNr());
                    this->esp->sendMsg(t.getSRC(), ret.genMsg(), ret.getMsgLength());
                    t.freePayload();
                    proccessRECData(t.getPayload(), t.getPayloadLength());
                    break;
                }
                default:
                    debugMSG("Toplevel Message doesn't seem to be useful");
                    break;
            }
        } else {
            if (compArr(t.tmpGetDest(), (unsigned char*) (broadcastAddr), 6)) {
                if (t.getType() == to_ping) {
                    TopMsg ret = TopMsg(to_ack, t.getSRC(), this->addr, t.getSeqNr());
                    this->esp->sendMsg(t.getSRC(), ret.genMsg(), ret.getMsgLength());
                    this->kH.add(t.getSRC());
                }
            } else {
                if (this->lrM.add(t.getSeqNr(), t.getSRC(), t.getDest())) {
                    this->esp->sendMsg(t.getDest(), c, i);
                }
            }
        }
    }
}

void PINS_Core::proccessRECData(unsigned char* c, unsigned int len) {
    std::ofstream out;
    out.open(DEMO_FILE, std::ios_base::app);
    out.write(reinterpret_cast<char*>(c), len);
    char c1 = '\n';
    out.write(&c1, 1);
    delete[] c;
}

void PINS_Core::intLoop() {
    unsigned int bcPingCount = 0;
    while (this->isRunning) {
        if (!this->espQueue.isEmpty()) {
            std::pair<unsigned char*, unsigned int> tmp = this->espQueue.pop();
            processMsg(tmp.first, tmp.second);
        }
        if (!this->ptQueue.isEmpty()) {
            std::pair<unsigned char*, unsigned int> tmp = this->ptQueue.pop();
            processMsg(tmp.first, tmp.second);
        }
        if (this->kH.size() == 0) {
            if (bcPingCount++ == 10) {
                lsM.addPing(this->smartbroadcastAddr);
                TopMsg t(to_ping, smartbroadcastAddr, this->addr, this->lsM.getPos());
                this->esp->sendMsg(smartbroadcastAddr, t.genMsg(), t.getMsgLength());
                debugMSG("Send broadcast ping");
                usleep(100000);
                bcPingCount = 0;
            } else {
                usleep(5000000);
            }
        }
        this->intTimer.setTimer();
    }
}

void PINS_Core::setAddress(unsigned char* address) {
    cpyMsgPart(address, this->addr.get()->data(), 0, 0, 6);
    this->esp->changeADDR(this->addr);
    this->pt->setAddr(this->addr);
}

void PINS_Core::startLoop() {
    infoMSG("Starting core loop");
    this->isRunning = true;
    if (this->esp->findESP() < 0) {
        errorMSG("can't find esp32");
    }
    this->esp->startLoop(&espQueue);
    this->pt->startLoop(&this->ptQueue, &this->lsM);
    if (this->pt->getState() != Connected) {
        errorMSG("Can't open socket");
    }
    this->t_loop = new std::thread(&PINS_Core::intLoop, this);
}

void PINS_Core::sendTOPMSG(ADDRESS dst, TopMsg t) {
    this->lsM.addMSG(dst, t.genMsg(), t.getMsgLength());
    this->esp->sendMsg(dst, t.genMsg(), t.getMsgLength());
}

void PINS_Core::sendMSG(ADDRESS dst, unsigned char* c, unsigned int len) {
    debugMSG("Adding new message with length: " + std::to_string(len));
    TopMsg t(to_sendbytes, dst, this->addr, this->lsM.getNextPos(), c, len);
    sendTOPMSG(dst, t);
}

void PINS_Core::sendBCMSG(unsigned char* c, unsigned int len) {
    sendMSG(this->smartbroadcastAddr, c, len);
}

void PINS_Core::stopLoop() {
    infoMSG("Stopping core loop");
    this->isRunning = false;
    if (this->t_loop != NULL) {
        this->t_loop->join();
        delete this->t_loop;
    }
    this->t_loop = NULL;
}
