#pragma once

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <vector>
#include <future>
#include <thread>
#include <atomic>
#include <utility>

#include "Logger.h"
#include "Consts.h"
#include "Enums.h"
#include "Cache.h"
#include "messages/ptm/PtMessage.h"
#include "messages/MessageQueue.h"


class PTCON {
 private:
    bool isRunning = false;
    int port;
    int sockfd;
    struct sockaddr_in address;
    ADDRESS hostAddress = std::make_shared<std::array<unsigned char, 6>>();
    unsigned int addrlen = sizeof(address);
    unsigned char* ack_Message;
    unsigned char* setAddr_Message;
    unsigned char* no_More_Messages;
    std::vector<std::pair<std::atomic<bool>*, std::thread*>> ptCHandler;
    unsigned int setAddr_Length;
    unsigned int ack_Length;
    unsigned int no_More_Length;
    int opt = 1;
    state intState;
    std::thread* t_loop = NULL;
    void intLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>*, LastSendMessages*);
    void connect();

    void closeOldCON();

 public:
    PTCON(unsigned int, ADDRESS);
    ~PTCON();
    state getState();
    void startLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>*, LastSendMessages*);
    void stopLoop();
    void setAddr(ADDRESS addr);
    void handleClient(int, MessageQueue<std::pair<unsigned char*, unsigned int>>*, LastSendMessages*, std::atomic<bool>*);
};
