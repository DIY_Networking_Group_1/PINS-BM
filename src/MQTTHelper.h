#pragma once

#if defined (__cplusplus)
extern "C" {
#endif

    #include "MQTTClient.h"
    #include "MQTTClientPersistence.h"

#if defined (__cplusplus)
}
#endif

#include <string>
#include <memory>
#include "Logger.h"
#include "PINS_Core.h"

#define MQTT_SERVER_ADDRESS "tcp://localhost:1883"
#define MQTT_TOPIC "sensors/#"
#define MQTT_QOS 1

typedef std::shared_ptr<std::array<unsigned char, 6>> KiliStpAddr;

// Based on: https://github.com/eclipse/paho.mqtt.c/blob/master/src/samples/MQTTClient_subscribe.c
class MQTTHelper {
 public:
    explicit MQTTHelper(PINS_Core* pc, KiliStpAddr localAddr);
    ~MQTTHelper();
    void start();
    void stop();
    bool isRunning();

 private:
    bool running = false;
    KiliStpAddr localAddr;
    MQTTClient client;
    MQTTClient_connectOptions conOpt = MQTTClient_connectOptions_initializer;
    PINS_Core* pc = NULL;

    bool startInternal();
    void stopInternal();
    static int onArrived(void* context, char* topicName, int topicLen, MQTTClient_message* message);
    static void onConnLost(void* context, char* cause);
    static void onDelivered(void* context, MQTTClient_deliveryToken dt);
    static std::string formatToJson(char* topic, int topicLength, char* data, int dataLength);
};
