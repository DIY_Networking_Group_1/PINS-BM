#pragma once

#include <unistd.h>
#include <thread>
#include <utility>
#include <memory>
#include <limits>
#include <fstream> // only needed for the demo purposes

#include "Timer.h"
#include "Cache.h"
#include "Logger.h"
#include "ESP32COM.h"
#include "PTCOM.h"
#include "messages/MessageTools.h"
#include "messages/MessageQueue.cpp"
#include "messages/MessageQueue.h"
#include "messages/toplevel/TopLevelMessage.h"


class PINS_Core {
 private:
    bool isRunning = false;
    ADDRESS addr = std::make_shared<std::array<unsigned char, 6>>();
    const ADDRESS smartbroadcastAddr = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>({0, 0, 0, 0, 0, 0}));
    const unsigned char broadcastAddr[6] = {0, 0, 0, 0, 0, 0};
    Timer intTimer = Timer(DEFAULT_SLEEP);
    ESP32COM* esp =  new ESP32COM();
    std::thread* t_loop = NULL;
    LastRecievedMessages lrM;
    LastSendMessages lsM;
    KnownHosts kH;
    PTCON* pt = NULL;
    MessageQueue<std::pair<unsigned char*, unsigned int>> espQueue;
    MessageQueue<std::pair<unsigned char*, unsigned int>> ptQueue;
    void intLoop();
    void processMsg(unsigned char*, unsigned int len);
    void sendTOPMSG(ADDRESS, TopMsg);
    void proccessRECData(unsigned char*, unsigned int);

 public:
    explicit PINS_Core(unsigned int port);
    ~PINS_Core();
    void sendMSG(ADDRESS, unsigned char*, unsigned int);
    void sendBCMSG(unsigned char*, unsigned int);
    void setAddress(unsigned char* address);
    void startLoop();
    void stopLoop();
};


