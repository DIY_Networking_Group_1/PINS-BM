#pragma once

#include <signal.h>
#include <iostream>
#include <string>
#include <cstring>

#include "messages/MessageTools.h"
#include "messages/BinaryHelper.h"
#include "Consts.h"
#include "Logger.h"
#include "ESP32COM.h"
#include "PINS_Core.h"
#include "MQTTHelper.h"

struct Args {
    unsigned int port = DEFAULT_PORT;
    std::string cache_folder = DEFAULT_CACHE_FOLDER;
    bool monitorMode = false;
    unsigned char* address = NULL;
    std::string serialPort = "";
};

PINS_Core* pc = NULL;
bool isTerminating = true;
void monitorMode(std::string port);
void signalHandler(int s);
void terminateProgram();
struct Args* parseParameter(int argc, char* const argv[]);
int main(int argc, char const *argv[]);
