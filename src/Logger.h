#pragma once

#include <iostream>
#include <string>

#define LOGLEVEL l_debug

enum LogLevel {
    l_none = 0,
    l_error = 1,
    l_warn = 2,
    l_info = 3,
    l_debug = 4
};

void debugMSG(const std::string);
void infoMSG(const std::string);
void warnMSG(const std::string);
void errorMSG(const std::string);
void parserMSG(unsigned char*, unsigned int);
