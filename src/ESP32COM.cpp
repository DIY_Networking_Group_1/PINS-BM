#include "ESP32COM.h"


namespace fs = std::experimental::filesystem;

ESP32COM::ESP32COM() {}

ESP32COM::ESP32COM(const std::string serialPort): SerialCon(serialPort, DEFAULT_BAUDRATE) {
    this->isConnected = this->SerialCon.Open() == 0 ? true : false;
}

ESP32COM::~ESP32COM() {
    if (!t_loop) {
        stopLoop();
    }
    if (this->isConnected) {
        this->SerialCon.Close();
    }
}

// todo implement method
bool ESP32COM::isCorrectESP() {
    return true;
}

int ESP32COM::findESP() {
    infoMSG("Trying to find ESP32");
    if (!this->isConnected) {
        for (int i = 0; i <= MAXSEARCH; i++) {
            const std::string addr = DEFAULTSERIALPREFIX + std::to_string(i);
            if (fs::exists(addr)) {
                this->SerialCon = Serial(addr, DEFAULT_BAUDRATE);
                if (this->SerialCon.Open() == 0) {
                    if (isCorrectESP()) {
                        this->isConnected = true;
                        return i;
                    }
                }
                this->SerialCon.Close();
            }
        }
    } else {
        return -1;
    }
    return -2;
}

void ESP32COM::updateScreen(unsigned char* c, unsigned int len) {
    if (this->isConnected) {
        SerialMsg s = SerialMsg(t_update_display, c, len);
        unsigned char* msg = s.genMsg();
        this->sendQueue.push(std::make_pair(msg, s.getMsgLength()));
    }
}

void ESP32COM::updateScreen(std::vector<unsigned char>* v) {
    updateScreen(v->data(), v->size());
}

void ESP32COM::sendMsg(ADDRESS dst, unsigned char* c, unsigned int len) {
    if (this->isConnected) {
        SerialMsg s = SerialMsg(t_send_Lora_data, dst, c, len);
        this->sendQueue.push(std::make_pair(s.genMsg(), s.getMsgLength()));
    }
}

void ESP32COM::sendMsg(ADDRESS dst, std::vector<unsigned char> *v) {
    sendMsg(dst, v->data(), v->size());
}

void ESP32COM::changeADDR(ADDRESS newADDR) {
    SerialMsg s = SerialMsg(t_set_Lora_address, newADDR);
    this->sendQueue.push(std::make_pair(s.genMsg(), s.getMsgLength()));
}

void ESP32COM::sendByte(unsigned char* c, unsigned int len) {
    debugMSG("Wrote: " + std::string(reinterpret_cast<char*>(c), len));
    this->SerialCon.Write(c, len);
}

void ESP32COM::Loop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out) {
    bool sendlock = true;
    unsigned char* c = new unsigned char[SERIAL_READ_BLOCK];
    unsigned int writeFairness = INTIAL_WRITE_FAIRNESS;
    while (this->isRunning) {
        unsigned int len = this->SerialCon.Read(c);
        if (len > 0) {
            SerialMsg s = SerialMsg(c, len);
            if (s.getState() == i_success) {
                switch (s.getType()) {
                    case t_send_buffer_full:
                        sendlock = true;
                        infoMSG("Locking write mechanism");
                        break;
                    case t_send_buffer_ready:
                        sendlock = false;
                        infoMSG("Unlocking write mechanism");
                        break;
                    case t_received_Lora_data:
                        out->push(std::pair<unsigned char*, unsigned int>(s.getPayload(), s.getPayloadLength()));
                        break;
                    default:
                        debugMSG("Serial Type doesn't seem useful");
                }
            } else {
                parserMSG(c, len);
            }
        }
        // unsigned int tmpSize = this->sendQueue.size();
        if (!sendlock) {
            for (unsigned int i = 0; i < writeFairness; i++) {
                if (!this->sendQueue.isEmpty()) {
                    std::pair<unsigned char*, unsigned int> tmp = this->sendQueue.pop();
                    sendByte(tmp.first, tmp.second);
                } else {
                    break;
                }
            }
        }
        // writeFairness = tmpSize + writeFairness < this->sendQueue.size() ? writeFairness + 1 : (tmpSize + writeFairness > this->sendQueue.size() ? (writeFairness -1 == 0 ? 1 : writeFairness -1) : writeFairness);
    }
    delete[] c;
    debugMSG("Terminating serial loop");
}

void ESP32COM::startLoop(MessageQueue<std::pair<unsigned char*, unsigned int>>* out) {
    debugMSG("starting esp32 loop");
    if (!this->isConnected) {
        findESP();
        if (!this->isConnected) {
            return;
        }
    }
    this->isRunning = true;
    this->t_loop = new std::thread(&ESP32COM::Loop, this, out);
}

void ESP32COM::stopLoop() {
    debugMSG("Stopping esp32 loop");
    this->isRunning = false;
    if (this->t_loop != NULL)
        this->t_loop->join();
    this->t_loop = NULL;
}

void ESP32COM::monitorMode() {
    if (!this->isConnected) {
        if (findESP() == -1) {
            errorMSG("Can't find ESP32");
            return;
        }
    }
    while (true) {
        std::cout << this->SerialCon.Read();
    }
}
