#include "Logger.h"


void debugMSG(const std::string msg) {
    if (LOGLEVEL >= l_debug)
        std::cout << "[DEBUG]: \t" << msg << "\n";
}

void  infoMSG(const std::string msg) {
    if (LOGLEVEL >= l_info)
        std::cout << "[INFO]: \t" << msg << "\n";
}

void  warnMSG(const std::string msg) {
    if (LOGLEVEL >= l_warn)
        std::cout << "[WARN]: \t" << msg << std::endl;
}

void  errorMSG(const std::string msg) {
    if (LOGLEVEL >= l_error)
        std::cout << "[ERROR]: \t" << msg << std::endl;
    exit(1);
}

void parserMSG(unsigned char* c, unsigned int len) {
    if (LOGLEVEL >= l_debug) {
        if (c[0] != '[') {
            std::cout << "[DEBUG]: Parsing Error: ";
        }
        std::cout.write(reinterpret_cast<char*>(c), len);
        if (len > 0 && c[len-1] != '\n') {
            std::cout << '\n';
        }
    }
}
