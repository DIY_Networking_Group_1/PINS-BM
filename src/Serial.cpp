#include "Serial.h"


Serial::Serial(const std::string serialPort, speed_t b) : SerialPort(serialPort) {
    this->baudrate = b;
    this->curState = Disconnected;
}

Serial::Serial(const std::string serialPort, speed_t b, unsigned int timeout): Serial(serialPort, b) {
    this->timeout = timeout;
}

Serial::~Serial() {
    if (this->curState == Connected || this->curState == Error)
        Close();
}

termios Serial::getTTY() {
    struct termios tty;
    memset(&tty, 0, sizeof(tty));
    if (tcgetattr(this->fd, &tty) != 0) {
        warnMSG("Can't get current settings");
    }
    return tty;
}

void Serial::configTTY(termios* tty) {
    tty->c_iflag = 0;
    tty->c_oflag = 0;
    tty->c_cflag = CS8|CREAD|CLOCAL|CSTOPB;
    tty->c_lflag = 0;
    tty->c_cc[VTIME] = (cc_t)(this->timeout / 100);
    tty->c_cc[VMIN] = 10;
    cfsetispeed(tty, this->baudrate);
    cfsetospeed(tty, this->baudrate);
}

int Serial::Open() {
    infoMSG("Trying to open " + this->SerialPort);
    this->fd = open(this->SerialPort.c_str(), O_RDWR);
    if (fd == -1) {
        warnMSG("Can't open " + this->SerialPort);
        this->curState = Error;
        return -2;
    }
    termios tty = getTTY();
    configTTY(&tty);
    tcflush(fd, TCIFLUSH);
    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        warnMSG("Can't apply settings to serial port");
        this->curState = Error;
        return -1;
    }
    this->curState = Connected;
    return 0;
}

void Serial::Close() {
    if (close(this->fd) != 0) {
        warnMSG("Can't close serial port");
        this->curState = Error;
    }
    this->curState = Disconnected;
}

int Serial::Read(unsigned char* c) {
    if (this->curState == Connected) {
        this->readTimer.setTimer();
        unsigned char* d = new unsigned char;
        bool r = true;
        int i;
        for (i = 0; i < SERIAL_READ_BLOCK; i++) {
            if (read(this->fd, d, 1) == 0) {
                r = false;
                break;
            }
            c[i] = *d;
            if (*d == '\n') {
                break;
            }
        }
        delete d;
        return r ? i+1 : i;
    }
    return -1;
}

const std::string Serial::Read() {
    unsigned char* c = new unsigned char[SERIAL_READ_BLOCK]{};
    std::string ret = "";
    int i;
    if (0 <= (i = Read(c))) {
        ret = std::string(reinterpret_cast<char*>(c), i);
    }
    delete[] c;
    return ret;
}

int Serial::Write(std::vector <unsigned char>* v) {
    return Write(v->data(), v->size());
}

int Serial::Write(unsigned char* c, unsigned int length) {
    if (this->curState == Connected) {
        int i = write(this->fd, c, length);
        if (c[length - 1] != '\n') {
            char c = '\n';
            write(this->fd, &c, 1);
            i++;
        }
        if (tcdrain(this->fd) == 0) {
            return i;
        }
    }
    return -1;
}

state Serial::getState() {
    return this->curState;
}
