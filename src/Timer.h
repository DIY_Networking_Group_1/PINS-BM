#pragma once

#include <unistd.h>
#include <chrono>


class Timer {
 private:
     std::chrono::time_point<std::chrono::system_clock> lastTime;
     unsigned int targetTimeout;
 public:
    explicit Timer(unsigned int timeout);
    ~Timer();
    void setTimer();
};
