#define CATCH_CONFIG_MAIN

#include <memory.h>
#include <iostream>
#include <string>
#include <random>

#include "lib/Catch2/single_include/catch2/catch.hpp"
#include "messages/BinaryHelper.h"
#include "messages/serial/SerialMessage.h"
#include "messages/toplevel/TopLevelMessage.h"
#include "messages/ptm/PtMessage.h"


TEST_CASE("Booleans are set to the correct value", "[boolean]") {
    REQUIRE(true == 1);
    REQUIRE(false == 0);
}

TEST_CASE("Uint64 with offset", "[BinaryHelper]") {
    std::mt19937_64 gen(std::random_device {}());
    for (int i = 0; i <= 1000; i++) {
        uint64_t comp = gen();
        unsigned int offset = gen() % 100;
        std::vector<unsigned char>* v = new std::vector<unsigned char>;
        setUint64(v, offset, comp);
        REQUIRE(comp == getUint64(v, offset));
        delete v;
        }
}

TEST_CASE("unsigned short with offset", "[BinaryHelper]") {
    std::mt19937_64 gen(std::random_device {}());
    for (int i = 0; i <= 1000; i++) {
        uint16_t comp = gen();
        unsigned int offset = gen() % 100;
        std::vector<unsigned char>* v = new std::vector<unsigned char>;;
        setUint16(v, offset, comp);
        REQUIRE(comp == getUint16(v, offset));
        delete v;
        }
}

TEST_CASE("unsigned int with offset", "[BinaryHelper]") {
    std::mt19937_64 gen(std::random_device {}());
    for (int i = 0; i <= 1000; i++) {
        unsigned int comp = gen();
        unsigned int offset = gen() % 100;
        std::vector<unsigned char>* v = new std::vector<unsigned char>;;
        setUint(v, offset, comp);
        REQUIRE(comp == getUint(v, offset));
        delete v;
        }
}

TEST_CASE("Testing getByteWithOffset", "[MessageTools]") {
    std::mt19937_64 gen(std::random_device {}());

    unsigned char data[1000];
    for (int i = 0; i < 1000; i++) {
        data[i] = gen();
    }

    for (int i = 0; i < 1000; i++) {
        REQUIRE(data[i] == getByteWithOffset(data, i * 8));
    }
}

TEST_CASE("Comparing two messages", "[MessageTools]") {
    unsigned char* c1 = (unsigned char*) "teststring";
    unsigned char* c2 = (unsigned char*) "teststring";
    unsigned char* c3 = (unsigned char*) "teststrink";
    REQUIRE(compArr(c1, c2, 11));
    REQUIRE(!compArr(c1, c3, 11));
}

TEST_CASE("Comparing two messages with offset", "[MessageTools]") {
    unsigned char* c1 = (unsigned char*) "fteststring";
    unsigned char* c2 = (unsigned char*) "teststrink";
    REQUIRE(compArr(c1, c2, 1, 0, 9));
    REQUIRE(!compArr(c1, c2, 1, 0, 10));
}

TEST_CASE("Comparing two messages with length 0", "[MessageTools]") {
    unsigned char* c1 = (unsigned char*) "not identical";
    unsigned char* c2 = (unsigned char*) "not really";
    REQUIRE(compArr(c1, c2, 0));
}

TEST_CASE("Comparing two messages with offset with length 0", "[MessageTools]") {
    unsigned char* c1 = (unsigned char*) "not identical";
    unsigned char* c2 = (unsigned char*) "not really";
    REQUIRE(compArr(c1, c2, 1, 2, 0));
}

TEST_CASE("try to copy one message into another with offset", "[MessageTools]") {
    unsigned char* c1 = (unsigned char*) "teststring";
    unsigned char* c2 = new unsigned char[7];
    cpyMsgPart(c1, c2, 4, 1, 6);
    REQUIRE(compArr(c1, c2, 4, 1, 6));
    delete[] c2;
}

TEST_CASE("SerialMsg compare works (with payload)", "[SerialMsg]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst2 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst3 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstat"});
    unsigned char* c1 = (unsigned char*) "content";
    unsigned char* c2 = (unsigned char*) "content";
    unsigned char* c3 = (unsigned char*) "contend";
    unsigned char* c4 = (unsigned char*) "contendd";

    SerialMsg s = SerialMsg(t_send_Lora_data, dst1, c1, 7);
    SerialMsg u = SerialMsg(t_send_Lora_data, dst2, c2, 7);
    SerialMsg g = SerialMsg(t_update_display, c3, 7);
    SerialMsg d = SerialMsg(t_update_display, c2, 7);
    SerialMsg e = SerialMsg(t_update_display, c4, 8);

    REQUIRE(s.getState() == i_success);
    REQUIRE(u.getState() == i_success);
    REQUIRE(g.getState() == i_success);
    REQUIRE(d.getState() == i_success);
    REQUIRE(e.getState() == i_success);

    REQUIRE(s.compare(&u));
    REQUIRE((!s.compare(&g)));
    REQUIRE(!s.compare(&d));
    REQUIRE(!s.compare(&e));
}

TEST_CASE("SerialMsg test t_received_Lora_data", "[SerialMsg]") {
    std::string s = "{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}";
    SerialMsg msg = SerialMsg(t_received_Lora_data, reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), s.size());
    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);

    SerialMsg msg2 = SerialMsg(msgSer, msg.getMsgLength());
    REQUIRE(msg2.getState() == i_success);
}

TEST_CASE("SerialMsg test t_send_Lora_data payload") {
    std::string s = "{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}";
    ADDR addr =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    SerialMsg msg = SerialMsg(t_send_Lora_data, addr, reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), s.size());

    REQUIRE(msg.getType() == t_send_Lora_data);
    REQUIRE(msg.getPayloadLength() == s.size());
    REQUIRE(compArr(reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), msg.getPayload(), s.size()));

    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);

    SerialMsg msg2 = SerialMsg(msgSer, msg.getMsgLength());
    REQUIRE(msg.getType() == msg2.getType());
    REQUIRE(msg2.getPayloadLength() == s.size());
    REQUIRE(compArr(reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), msg2.getPayload(), s.size()));
}

TEST_CASE("SerialMsg test t_received_Lora_data payload") {
    std::string s = "{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}";
    SerialMsg msg = SerialMsg(t_received_Lora_data, reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), s.size());

    REQUIRE(msg.getPayloadLength() == s.size());
    REQUIRE(compArr(reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), msg.getPayload(), s.size()));

    uchar* msgSer = new uchar[msg.getMsgLength()];
    msg.genMsg(msgSer);

    SerialMsg msg2 = SerialMsg(msgSer, msg.getMsgLength());
    REQUIRE(msg2.getPayloadLength() == s.size());
    REQUIRE(compArr(reinterpret_cast<uchar*>(const_cast<char*>(s.c_str())), msg2.getPayload(), s.size()));
}

TEST_CASE("SerialMsg compare works (mixed)", "[SerialMsg]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst2 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});

    unsigned char* c1 =(unsigned char*) "contet";

    SerialMsg b = SerialMsg(t_send_Lora_data, dst1, c1, 7);
    SerialMsg c = SerialMsg(t_update_display, c1, 7);

    REQUIRE(b.getState() == i_success);
    REQUIRE(c.getState() == i_success);

    REQUIRE(!b.compare(&c));
}

TEST_CASE("SerialMsg without Payload, without addr", "[SerialMsg]") {
    SerialMsg s = SerialMsg(t_send_buffer_full);
    REQUIRE(s.getState() == i_success);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();
    SerialMsg* s2 = new SerialMsg(c, len);

    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.compare(s2));

    delete s2;
    delete[] c;
}

TEST_CASE("SerialMsg Creating Message and parsing it again, without addr", "[SerialMsg]") {
    unsigned char* g = new unsigned char[1024];

    SerialMsg s = SerialMsg(t_update_display, g, 1024);
    REQUIRE(s.getState() == i_success);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();
    SerialMsg* s2 = new SerialMsg(c, len);

    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.compare(s2));

    delete s2;
    delete[] c;
    delete[] g;
}

TEST_CASE("SerialMsg Creating Message and parsing it again, with addr", "[SerialMsg]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    unsigned char* g = new unsigned char[1024];

    SerialMsg s = SerialMsg(t_send_Lora_data, dst1, g, 1024);
    REQUIRE(s.getState() == i_success);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();

    SerialMsg* s2 = new SerialMsg(c, len);

    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.compare(s2));
    delete s2;
    delete[] c;
    delete[] g;
}

TEST_CASE("Top Msg compare works (without payload)", "[TopMsg]") {
    ADDR src1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    TopMsg s = TopMsg(to_ping, src1, dst1, 2);
    TopMsg s2 = TopMsg(to_ping, src1, dst1, 2);
    TopMsg f = TopMsg(to_ack, src1, dst1, 2);

    REQUIRE(s.getState() == i_success);
    REQUIRE(s2.getState() == i_success);
    REQUIRE(f.getState() == i_success);

    REQUIRE(s.strictCompare(&s2));
    REQUIRE(!s.strictCompare(&f));
}

TEST_CASE("Serial and TopMsg combined test", "[TopMsg && SerialMsg]") {
    std::string s = "{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}";
    ADDR src1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    TopMsg t1(to_sendbytes, src1, dst1, 0, reinterpret_cast<unsigned char*>(const_cast<char*>(s.c_str())), s.size());
    REQUIRE(t1.getState() == i_success);
    unsigned char* tpay = t1.genMsg();
    SerialMsg s1(t_received_Lora_data, tpay, t1.getMsgLength());
    REQUIRE(s1.getState() == i_success);
    unsigned char* spay = s1.genMsg();

    SerialMsg so1(spay, s1.getMsgLength());
    REQUIRE(so1.getState() == i_success);
    REQUIRE(s1.compare(&so1));

    TopMsg to1(so1.getPayload(), so1.getPayloadLength());
    REQUIRE(to1.getState() == i_success);
    REQUIRE(t1.strictCompare(&to1));

    delete[] tpay;
    delete[] spay;
}

TEST_CASE("Top Msg compare works (changing addresses)", "[TopMsg]") {
    ADDR src1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR src2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR src3 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcar"});
    ADDR dst3 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstar"});
    TopMsg s = TopMsg(to_ping, src1, dst1, 2);
    TopMsg s2 = TopMsg(to_ping, src2, dst2, 2);
    TopMsg f = TopMsg(to_ping, src3, dst2, 2);
    TopMsg g = TopMsg(to_ping, src2, dst3, 2);
    TopMsg h = TopMsg(to_ping, src3, dst3, 2);

    REQUIRE(s.getState() == i_success);
    REQUIRE(g.getState() == i_success);
    REQUIRE(f.getState() == i_success);
    REQUIRE(h.getState() == i_success);
    REQUIRE(s2.getState() == i_success);

    REQUIRE(!s.strictCompare(&f));
    REQUIRE(s.strictCompare(&s2));
    REQUIRE(!s.strictCompare(&g));
    REQUIRE(!s.strictCompare(&h));
}

TEST_CASE("Top Msg compare works (changing SEQNr)", "[TopMsg]") {
    ADDR src1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"srcad"});
    ADDR dst1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"dstad"});
    ADDR src2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"srcad"});
    ADDR dst2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"dstad"});
    TopMsg s = TopMsg(to_ping, src1, dst1, 2);
    TopMsg g = TopMsg(to_ping, src2, dst2, 2);
    TopMsg f = TopMsg(to_ping, src2, dst2, 3);

    REQUIRE(s.getState() == i_success);
    REQUIRE(g.getState() == i_success);
    REQUIRE(f.getState() == i_success);

    REQUIRE(!s.strictCompare(&f));
    REQUIRE(s.strictCompare(&g));
    REQUIRE(s.compare(&g));
    REQUIRE(s.compare(&f));
}

TEST_CASE("TopMsg compare works (with payload)", "[TopMsg]") {
    ADDR src1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"srcad"});
    ADDR dst1 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"dstad"});
    ADDR src2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"srcad"});
    ADDR dst2 = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"dstad"});
    unsigned char* c1 = (unsigned char*) "content";
    unsigned char* c2 = (unsigned char*) "content";
    unsigned char* c3 = (unsigned char*) "contend";
    unsigned char* c4 = (unsigned char*) "contendd";
    TopMsg s = TopMsg(to_sendbytes, src1, dst1, 2, c1, 7);
    TopMsg g = TopMsg(to_sendbytes, src2, dst2, 2, c2, 7);
    TopMsg f = TopMsg(to_sendbytes, src2, dst2, 2, c3, 7);
    TopMsg h = TopMsg(to_sendbytes, src2, dst2, 2, c4, 7);
    TopMsg j = TopMsg(to_sendbytes, src2, dst2, 2, c4, 8);

    REQUIRE(s.getState() == i_success);
    REQUIRE(g.getState() == i_success);
    REQUIRE(f.getState() == i_success);
    REQUIRE(h.getState() == i_success);
    REQUIRE(j.getState() == i_success);

    REQUIRE(s.strictCompare(&g));
    REQUIRE(!s.strictCompare(&h));
    REQUIRE(!s.strictCompare(&f));
    REQUIRE(!s.strictCompare(&j));
}

TEST_CASE("TopMsg Creating Message and parsing it again", "[TopMsg]") {
    unsigned char* g = new unsigned char[1024];
    ADDR src = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst = std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6> {"dstad"});
    TopMsg s = TopMsg(to_sendbytes , src, dst, 1,  g, 1024);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();
    TopMsg* s2 = new TopMsg(c, len);

    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.strictCompare(s2));

    delete s2;
    delete[] c;
    delete[] g;
}

TEST_CASE("PtMessage compare works (with payload)", "[PtMessage]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst2 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst3 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstat"});
    unsigned char* c1 = (unsigned char*) "content";
    unsigned char* c2 = (unsigned char*) "content";
    unsigned char* c3 = (unsigned char*) "contend";
    unsigned char* c4 = (unsigned char*) "contendd";

    PtMessage s = PtMessage(pt_send_message, dst1, c1, 7);
    PtMessage u = PtMessage(pt_send_message, dst2, c2, 7);
    PtMessage g = PtMessage(pt_send_message, dst3, c2, 7);
    PtMessage d = PtMessage(pt_send_message, dst2, c3, 7);
    PtMessage e = PtMessage(pt_send_message, dst2, c4, 8);
    PtMessage f = PtMessage(pt_send_message, dst2, c4, 7);

    REQUIRE(s.getState() == i_success);
    REQUIRE(u.getState() == i_success);
    REQUIRE(g.getState() == i_success);
    REQUIRE(d.getState() == i_success);
    REQUIRE(e.getState() == i_success);
    REQUIRE(f.getState() == i_success);

    REQUIRE(s.compare(&u));
    REQUIRE((!s.compare(&g)));
    REQUIRE(!s.compare(&d));
    REQUIRE(!s.compare(&e));
    REQUIRE(!s.compare(&f));
}

TEST_CASE("PtMessage compare works (mixed)", "[PtMessage]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    ADDR dst2 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});

    unsigned char* c1 =(unsigned char*) "contet";
    PtMessage a = PtMessage(pt_send_base_addr, dst2);
    PtMessage b = PtMessage(pt_send_message, dst1, c1, 7);

    REQUIRE(a.getState() == i_success);
    REQUIRE(b.getState() == i_success);

    REQUIRE(!a.compare(&b));
}

TEST_CASE("PtMessage without Payload, without addr", "[PtMessage]") {
    PtMessage s = PtMessage(pt_ptm_full);
    REQUIRE(s.getState() == i_success);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();
    PtMessage* s2 = new PtMessage(c, len);
    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.compare(s2));
    delete s2;
    delete[] c;
}

TEST_CASE("PtMessage Creating Message and parsing it again, with addr", "[PtMessage]") {
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    unsigned char* g = new unsigned char[1024];

    PtMessage s = PtMessage(pt_send_message, dst1, g, 1024);
    REQUIRE(s.getState() == i_success);
    unsigned char* c = s.genMsg();
    unsigned int len = s.getMsgLength();
    PtMessage* s2 = new PtMessage(c, len);
    REQUIRE(s2->getState() == i_success);
    REQUIRE(s.compare(s2));
    delete s2;
    delete[] c;
    delete[] g;
}

TEST_CASE("PtMessage and TopMsg combined test", "[TopMsg && Ptmessage]") {
    std::string s = "{ \"topic\": \"sensors/thermal/b8:27:eb:ef:be:01\", \"data\": \"23.75\"}";
    ADDR src1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"srcad"});
    ADDR dst1 =  std::make_shared<std::array<unsigned char, 6>>(std::array<unsigned char, 6>{"dstad"});
    TopMsg t1(to_sendbytes, src1, dst1, 0, reinterpret_cast<unsigned char*>(const_cast<char*>(s.c_str())), s.size());
    REQUIRE(t1.getState() == i_success);
    unsigned char* tpay = t1.genMsg();
    PtMessage s1(pt_send_message, dst1, tpay, t1.getMsgLength());
    REQUIRE(s1.getState() == i_success);
    unsigned char* spay = s1.genMsg();

    PtMessage so1(spay, s1.getMsgLength());
    REQUIRE(so1.getState() == i_success);
    REQUIRE(s1.compare(&so1));

    TopMsg to1(so1.getPayload(), so1.getPayloadLength());
    REQUIRE(to1.getState() == i_success);
    REQUIRE(t1.strictCompare(&to1));

    delete[] tpay;
    delete[] spay;
}
