G++_COMPILER=g++
G++_COMPILER_PI=/usr/local/gcc-8.1.0/bin/g++-8.1.0
BUILD_DIR=build
DEBUG_DIR=debug
BUILD_NAME=pins
TEST_NAME=$(BUILD_NAME)_test
MAIN_CPP=src/PINS_BM.cpp
MAIN_TEST=test/000-mainTest.cpp
ADDITONAL_CPP=src/Serial.cpp src/ESP32COM.cpp src/Logger.cpp src/PINS_Core.cpp src/messages/BinaryHelper.cpp src/PTCOM.cpp src/messages/MessageTools.cpp src/messages/serial/SerialMessage.cpp src/messages/toplevel/TopLevelMessage.cpp src/Cache.cpp src/messages/MessageQueue.cpp src/Timer.cpp src/messages/ptm/PtMessage.cpp src/MQTTHelper.cpp
INCLUDE_DIR=-Isrc/ -Isrc/messages/
ADDITIONAL_LIBS=-lpaho-mqtt3c -lstdc++fs
ADDITIONAL_OPTIONS=$(ADDITIONAL_LIBS) -std=c++17 -pthread

default:
	make clean
	make debug

init:
	if [ -d src/messages/ ]; then rm -rf src/messages; fi
	if [ -d test/lib/Catch2 ]; then rm -rf test/lib/Catch2; fi
	git submodule init
	git submodule update --recursive --remote

run:
	make default
	./$(DEBUG_DIR)/PINS_BM

compileCommands:
	mkdir -p $(DEBUG_DIR)
	clang++ -MJ  a.o.json $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)
	clang++ -MJ  b.o.json $(MAIN_TEST) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(TEST_NAME) $(ADDITIONAL_OPTIONS)
	sed -e '1s/^/[\n/' -e '$$s/,$$/\n]/' *.o.json > compile_commands.json
	rm *.o.json

clean:
	# Only remove the build folder if it exists:
	if [ -d $(BUILD_DIR) ]; then rm -rf $(BUILD_DIR); fi
	if [ -d $(DEBUG_DIR) ]; then rm -rf $(DEBUG_DIR); fi

compile:
	mkdir -p $(BUILD_DIR)
	${G++_COMPILER} -oo $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(BUILD_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

compilePi:
	mkdir -p $(BUILD_DIR)
	${G++_COMPILER_PI} -oo $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(BUILD_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

debug:
	mkdir -p $(DEBUG_DIR)
	${G++_COMPILER} -g -o0 $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

UnitTests:
	make clean
	make compileTest
	make runTests

compileTest:
	mkdir -p $(DEBUG_DIR)
	${G++_COMPILER} -o0 -g $(MAIN_TEST) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(TEST_NAME) $(ADDITIONAL_OPTIONS)

runTests:
	./$(DEBUG_DIR)/$(TEST_NAME)

install:
	mv $(BUILD_DIR)/$(BUILD_NAME) /opt/
