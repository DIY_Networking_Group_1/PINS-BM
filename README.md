# PINS-BM (**B**ase **M**ode)

## Description:
C/C++ The base station for pins. It is the hub were all connections arrive ([PINS-Lora-transfer](https://gitlab.com/DIY_Networking_Group_1/PINS-Lora-transfer), [PTM-Device](https://gitlab.com/DIY_Networking_Group_1/PINS-PTM), [Mosquito](https://mosquitto.org/)). Those connections are used to exchange messages to other endpoints in Base Mode. The Packages which should be transferred receives the Base station via Mosquito. The recieved data is appended to the  [file](https://gitlab.com/DIY_Networking_Group_1/PINS-BM/blob/master/src/Consts.h#L38).

## Dependencies:

To compile PINS-BM you need a C++17 capable compiler ([G++ >= 7](https://gcc.gnu.org/projects/cxx-status.html) , [Clang++ >= 6](https://clang.llvm.org/cxx_status.html)).
For Buildsystems you need cmake >= 3

For the Mosquito communication it also needs paho.mqtt.

## Installation:
To be able to build the the **PINS-BM** project with g++, the following steps have to be done before:

1. Clone the repository:
```
git clone --recursive https://gitlab.com/DIY_Networking_Group_1/PINS-BM.git
cd PINS-BM
```
2. Update submodules (optional):

Normally you don't need to updated the submodules. But if building is failing you could try to update them.
```
git submodule init
git submodule update --recursive --remote
```
3. Install [paho.mqtt.c](https://github.com/eclipse/paho.mqtt.c):

*Under Debian based systems:*
```
sudo apt install libssl-dev fakeroot fakeroot devscripts dh-make lsb-release build-essential gcc make cmake cmake-gui cmake-curses-gui
git clone https://github.com/eclipse/paho.mqtt.c.git
cd paho.mqtt.c
make
sudo make install
cd ..
```
*Under Fedora based Systems*
```
sudo dnf install paho-c-devel
```
4. Build the project:

*New(recommended):*
```
mkdir build
cd build
cmake ..
make
```

*Old:*

```
make compile
```

5. Install

This step isn't mandatory, it will only copy the compiled pins file into **/opt/**.
```
make install
```
